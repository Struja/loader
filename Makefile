#Directories
DIST_DIR=./dist
TEST_BIN=./testbin
TEST_SRC=./testsrc
SRC=src

#Log files
OBJDUMP_OUTPUT=obj_dump_log.txt
READELF_OUTPUT=readelf_log.txt

#Test files
TEST_INPUT=test
TEST_OUTPUT=test

#Source files
OUTPUT=loader
INPUT=loader.c

#Tools for build and dump
CC=arm-linux-gnueabi-gcc
AS=arm-linux-gnueabi-as
OBJDUMP=arm-linux-gnueabi-objdump
READELF=arm-linux-gnueabi-readelf

#Flags used for build
FLAGS=-static -march=armv5 -w

#Default tasks
all: clean mkdir tests dump readelf build run

#Builds test files
tests:
	$(AS) $(TEST_SRC)/$(TEST_INPUT)1.s -o $(TEST_BIN)/$(TEST_OUTPUT)1.o
	$(AS) $(TEST_SRC)/$(TEST_INPUT)2.s -o $(TEST_BIN)/$(TEST_OUTPUT)2.o
	$(AS) $(TEST_SRC)/$(TEST_INPUT)3.s -o $(TEST_BIN)/$(TEST_OUTPUT)3.o
 
#Dumps the content of test files
dump:
	$(OBJDUMP) -d -r  $(TEST_BIN)/$(TEST_OUTPUT)1.o >  $(DIST_DIR)/$(OBJDUMP_OUTPUT)
	$(OBJDUMP) -d -r  $(TEST_BIN)/$(TEST_OUTPUT)2.o >> $(DIST_DIR)/$(OBJDUMP_OUTPUT)
	$(OBJDUMP) -d -r  $(TEST_BIN)/$(TEST_OUTPUT)3.o >> $(DIST_DIR)/$(OBJDUMP_OUTPUT)

#Dumps the readelf output of test files
readelf:
	$(READELF) -a $(TEST_BIN)/$(TEST_OUTPUT)1.o >  $(DIST_DIR)/$(READELF_OUTPUT)
	$(READELF) -a $(TEST_BIN)/$(TEST_OUTPUT)2.o >> $(DIST_DIR)/$(READELF_OUTPUT)
	$(READELF) -a $(TEST_BIN)/$(TEST_OUTPUT)3.o >> $(DIST_DIR)/$(READELF_OUTPUT)

#Runs the project
run:
	clear
	$(DIST_DIR)/$(OUTPUT)

#Builds the project
build:
	$(CC) $(SRC)/$(INPUT) $(FLAGS) -o $(DIST_DIR)/$(OUTPUT) 

#Makes the $DIST_DIR
mkdir: 
	mkdir $(DIST_DIR)

#Cleans the $DIST_DIR
clean:
	rm -rf $(DIST_DIR)
	rm -rf $(TEST_BIN)/*

