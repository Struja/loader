.section .text
	.extern printf
	.extern scanf

	.global main  
.func
main:   
	push {ip, lr}

	ldr r0, =instr1  
	bl printf
	ldr r1, =num1
	ldr r0, =scan_format
	bl scanf

	ldr r0, =instr2  
	bl printf
	ldr r1, =num2
	ldr r0, =scan_format
	bl scanf

	ldr r4, =num1
	ldr r4, [r4]
	ldr r5, =num2
	ldr r5, [r5]

	add r1, r4, r5
	sub r2, r4, r5
	mul r3, r4, r5

	ldr r0, =out_format
	bl printf

	pop {ip, pc}
.endfunc

.section .data
scan_format:    .asciz "%d"
out_format: 	.asciz "Sum: %d    Difference: %d    Product: %d\n"

instr1: .asciz "Enter first integer: "
instr2: .asciz "Enter second integer: "

num1: .long 0
num2: .long 0 

.end