.section .text
    .extern printf

    .global  main
.func
main:          
    push {lr}   
    
    cmp r0, #1

    beq noname

    add r1, r1, #16
    ldr r0, =hello 
    b print

noname:
    ldr r0, =error 
   
print:
    bl printf  
    
    mov r0, #0

    pop {pc}   
.endfunc

.section .data 
.align 2
hello:  .asciz "Hello %s!\n"       
error:  .asciz "Next time enter name.\n"      

.end