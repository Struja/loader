.section .text
    .extern printf
    .global  main
.func
main:         
    push {lr}   
    
    push {r0}
    
    pop {r1}

    ldr r0, =izlaz 
    bl printf 
    
    mov r0, #0

    pop {pc}    
.endfunc

.section .data
.align 2
izlaz:  .asciz "Arguments count: %d\n"       @ izlazni format
.end
