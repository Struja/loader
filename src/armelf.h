#ifndef __ASMARM_ELF_H
#define __ASMARM_ELF_H

/*
 * ELF register definitions..
 */

struct task_struct;

typedef unsigned long elf_greg_t;
typedef unsigned long elf_freg_t[3];

#define ELF_NGREG (sizeof (struct pt_regs) / sizeof(elf_greg_t))
//typedef elf_greg_t elf_gregset_t[ELF_NGREG];

typedef struct user_fp elf_fpregset_t;

#define EM_ARM  40

#define EF_ARM_EABI_MASK        0xff000000
#define EF_ARM_EABI_UNKNOWN     0x00000000
#define EF_ARM_EABI_VER1        0x01000000
#define EF_ARM_EABI_VER2        0x02000000
#define EF_ARM_EABI_VER3        0x03000000
#define EF_ARM_EABI_VER4        0x04000000
#define EF_ARM_EABI_VER5        0x05000000

#define EF_ARM_BE8              0x00800000      /* ABI 4,5 */
#define EF_ARM_LE8              0x00400000      /* ABI 4,5 */
#define EF_ARM_MAVERICK_FLOAT   0x00000800      /* ABI 0 */
#define EF_ARM_VFP_FLOAT        0x00000400      /* ABI 0 */
#define EF_ARM_SOFT_FLOAT       0x00000200      /* ABI 0 */
#define EF_ARM_OLD_ABI          0x00000100      /* ABI 0 */
#define EF_ARM_NEW_ABI          0x00000080      /* ABI 0 */
#define EF_ARM_ALIGN8           0x00000040      /* ABI 0 */
#define EF_ARM_PIC              0x00000020      /* ABI 0 */
#define EF_ARM_MAPSYMSFIRST     0x00000010      /* ABI 2 */
#define EF_ARM_APCS_FLOAT       0x00000010      /* ABI 0, floats in fp regs */
#define EF_ARM_DYNSYMSUSESEGIDX 0x00000008      /* ABI 2 */
#define EF_ARM_APCS_26          0x00000008      /* ABI 0 */
#define EF_ARM_SYMSARESORTED    0x00000004      /* ABI 1,2 */
#define EF_ARM_INTERWORK        0x00000004      /* ABI 0 */
#define EF_ARM_HASENTRY         0x00000002      /* All */
#define EF_ARM_RELEXEC          0x00000001      /* All */

#define R_ARM_NONE              0
#define R_ARM_PC24              1
#define R_ARM_ABS32             2
#define R_ARM_CALL              28
#define R_ARM_JUMP24            29
#define R_ARM_V4BX              40
#define R_ARM_PREL31            42
#define R_ARM_MOVW_ABS_NC       43
#define R_ARM_MOVT_ABS          44

#define R_ARM_THM_CALL          10
#define R_ARM_THM_JUMP24        30
#define R_ARM_THM_MOVW_ABS_NC   47
#define R_ARM_THM_MOVT_ABS      48


#endif

