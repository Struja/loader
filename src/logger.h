#ifndef __errors_h__
#define __errors_h__

#define ERROR_OPEN_FILE 1
#define ERROR_BAD_FILE_FORMAT 2
#define ERROR_BAD_ELF_CLASS 3
#define ERROR_ELF_TYPE 4
#define ERROR_ELF_VERSION 5
#define ERROR_ELF_VERSION 8
#define ERROR_MMAP 6
#define ERROR_MUNMAP 7



static char* ERRORS[] = {
                            "Can't open file!",
                            "Not ELF file!",
                            "Not 32-bit ELF",
                            "ELF not relocatable",
                            "Invalid ELF version",
                            "mmap() fails...",
                            "munmap() fails...",
                            "Not ARM machine"
			};

#define error(X) printf("\nError: %s\n", ERRORS[X-1]); return(X);


#define DEBUG_FLAG 0

#define DEBUG_Elf32_Shdr(X, Y) printf("\nElf32_Shdr\n\
sh_addr:\t%4xh\n\
sh_addralign:\t%4xh\n\
sh_entsize:\t%4xh\n\
sh_flags:\t%4xh\n\
sh_info:\t%4xh\n\
sh_link:\t%4xh\n\
sh_name:\t%s\n\
sh_offset:\t%4xh\n\
sh_size:\t%4xh\n\
sh_type:\t%4xh\n",\
X.sh_addr, \
X.sh_addralign, \
X.sh_entsize, \
X.sh_flags, \
X.sh_info, \
X.sh_link, \
Y + X.sh_name, \
X.sh_offset, \
X.sh_size, \
X.sh_type);

#define DEBUG_Elf32_Ehdr(X) printf("\nElf32_Ehdr\n\
e_type:\t\t%4xh\n\
e_machine:\t%4xh\n\
e_version:\t%4xh\n\
e_entry:\t%4xh\n\
e_phoff:\t%4xh\n\
e_shoff:\t%4xh\n\
e_flags:\t%4xh\n\
e_ehsize:\t%4xh\n\
e_phentsize:\t%4xh\n\
e_phnum:\t%4xh\n\
e_shentsize:\t%4xh\n\
e_shnum:\t%4xh\n\
e_shstrndx:\t%4xh\n",\
X.e_type,\
X.e_machine,\
X.e_version,\
X.e_entry,\
X.e_phoff,\
X.e_shoff,\
X.e_flags,\
X.e_ehsize,\
X.e_phentsize,\
X.e_phnum,\
X.e_shentsize,\
X.e_shnum,\
X.e_shstrndx);

#define DEBUG_Elf32_Sym_HEADERS() printf("\nElf32_Sym\n\
st_name|\
st_value|\
st_size|\
st_bind|\
st_type|\
st_other|\
st_shndx|\t");

#define DEBUG_Elf32_Sym(X, Y, i) printf("\n\
%s\t\
%8xh\
%4xh\t\
%4xh\t\
%4xh\t\
%4xh\t\
%4xh\t\
%4xh\t",\
&Y[X.st_name],\
X.st_value,\
X.st_size,\
((X.st_info)>>4),\
((X.st_info)&0xf),\
X.st_other,\
X.st_shndx,\
i);

#define DEBUG_Elf32_Rel_HEADERS() printf("\nElf32_Rel\n\
r_offset:\t\
ELF32_R_SYM:\t\
ELF32_R_TYPE:\t");

#define DEBUG_Elf32_Rel(X) printf("\n\
%4xh\t\t\
%4xh\t\t\
%4xh\t\t",\
X.r_offset,\
X.r_info>>8,\
(unsigned char)(X.r_info));



#endif


