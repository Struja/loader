#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "elf.h"
#include "logger.h"
#include "armelf.h"

#define ENTRY_POINT "main"
#define STACK_SIZE 1024*1024

#define PROT_ALLOW_ALL PROT_WRITE | PROT_READ | PROT_EXEC

#define PAGE_ALIGN(X) X & ~(sysconf(_SC_PAGE_SIZE) - 1)
#define PAGE_OFFSET(X) X % (sysconf(_SC_PAGE_SIZE))

#define FREAD(pointer, file, offset, size, count) \
fseek(file, offset, SEEK_SET); \
fread(pointer, size, count, file);

#define REPORT_ERROR(msg) \
do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define SCANF scanf
#define PRINTF printf
#define LDR_PC_PC 0xe59ff000

#define PRINTF_SYMBOL "printf"
#define SCANF_SYMBOL "scanf"

int main(void) {

    /*
     * Iterators
     */
    int i, j;

    /* 
     * Parse input params from stdin 
     */

    int (*_main)(int, char**);

    char* old_stack;
    char* new_stack = malloc(STACK_SIZE);

while(1) {
    char params[256];
    int argc = 0;
    char argv[16][16];

    gets (params);

    char* token;
    token = strtok(params, " ");
    while (token) {
        strcpy(argv[argc++], token);
        token = strtok(NULL, " ");
    }

    if (argc == 0) {
        continue;
    }

    /*
     * Opens input files
     */
    FILE* input = NULL;
    int fd = -1;
    input = fopen(argv[0], "rw");
    fd = open(argv[0], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

    if (NULL == input) {
        error(ERROR_OPEN_FILE);
    }

    /* 
     * Reading elf file header
     */
    Elf32_Ehdr header;

    FREAD(&header, input, 0, 1, sizeof (header));

    if(DEBUG_FLAG) {
        DEBUG_Elf32_Ehdr(header);
    }

    /*
     * Checks if elf header has appropriate format.
     * It should be 32bit relocatable elf file for ARM machine and have right
     * MAG and version fields.
     */
    if (strncmp((char*) header.e_ident, ELFMAG, SELFMAG)) {
        error(ERROR_BAD_FILE_FORMAT);
    }
    if (header.e_ident[EI_CLASS] != ELFCLASS32) {
        error(ERROR_BAD_ELF_CLASS);
    }
    if (header.e_type != ET_REL) {
        error(ERROR_ELF_TYPE);
    }
    if (header.e_version != EV_CURRENT) {
        error(ERROR_ELF_VERSION);
    }
    if (header.e_machine != EM_ARM) {
        error(ERROR_ELF_VERSION);
    }

    /* 
     * Loading section headers.
     */
    Elf32_Shdr shdrs[header.e_shnum];
    FREAD(shdrs, input, header.e_shoff, sizeof shdrs[0], header.e_shnum);

    /*
     * Loading string section with section names.
     */
    Elf32_Shdr* shdr_shstr = &shdrs[header.e_shstrndx];
    char sh_shstr[shdr_shstr->sh_size];
    FREAD(sh_shstr, input, shdr_shstr->sh_offset, 1, shdr_shstr->sh_size);

    /*
     * Mapping sections into the memory if section is allocatable and has size 
     * different than zero. Permissions are set to RWE and mapping is private 
     * which means that allocated memory will be visible to this process only. 
     * Addresses are aligned to page size so we can set access rights later. 
     * After allocation section address field is updated and used as a pointer
     * to virtual address. In case that mapping fails method will report error.
     */
    for (i = 0; i < header.e_shnum; ++i) {
        if ((SHF_ALLOC & shdrs[i].sh_flags) && shdrs[i].sh_size) {
            Elf32_Shdr* shdr_ptr = &shdrs[i];

            Elf32_Word pa_size = shdr_ptr->sh_size;
            Elf32_Off pa_offset;
            void* addr;

            if (shdr_ptr->sh_type == SHT_NOBITS) {
                shdr_ptr->sh_offset = 0;
            }
            pa_size += PAGE_OFFSET(shdr_ptr->sh_offset);

            /**
             *Additional space used for prinf and scanf jumps
             */
            if(SHF_EXECINSTR & shdr_ptr->sh_flags) {
                pa_size += 4 * sizeof(unsigned);
            }

            pa_offset = PAGE_ALIGN(shdr_ptr->sh_offset);

            addr = mmap(NULL, pa_size, PROT_ALLOW_ALL, MAP_PRIVATE, fd, pa_offset);

            if (addr != MAP_FAILED) {
                shdr_ptr->sh_addr = addr + PAGE_OFFSET(shdr_ptr->sh_offset);
                shdr_ptr->sh_size = pa_size;

                if(SHF_EXECINSTR & shdr_ptr->sh_flags) {
                    unsigned* sh_end_ptr = addr + pa_size;
                    *(sh_end_ptr - 4) = LDR_PC_PC;
                    *(sh_end_ptr - 3) = LDR_PC_PC;
                    *(sh_end_ptr - 2) = SCANF;
                    *(sh_end_ptr - 1) = PRINTF;
                    if (DEBUG_FLAG) {
                        printf("\n\n  %x %x %x %x %x %x\n", 
                            sh_end_ptr - 4, sh_end_ptr - 3, 
                            *(sh_end_ptr - 2), *(sh_end_ptr - 1), scanf, printf);
                    }
                }
            } else {
               REPORT_ERROR("mmap");
            }
        }
    }


    /* 
     * Searching for symbols header and loading symbols section, with setting
     * up variable that holds symbols count.
     */
    Elf32_Shdr* shdr_sym;

    for (i = 0; i < header.e_shnum; i += 1) {
        if (shdrs[i].sh_type == SHT_SYMTAB) {
            shdr_sym = &shdrs[i];
            break;
        }
    }

    int sh_sym_entnum = shdr_sym->sh_size / shdr_sym->sh_entsize;

    Elf32_Sym sh_sym[sh_sym_entnum];
    FREAD(sh_sym, input, shdr_sym->sh_offset, 1, shdr_sym->sh_size);

    /* 
     * Loading strings section with names of the symbols.
     */
    Elf32_Shdr* shdr_symstr = &shdrs[shdr_sym->sh_link];
    char sh_symstr[shdr_symstr->sh_size];
    FREAD(sh_symstr, input, shdr_symstr->sh_offset, 1, shdr_symstr->sh_size);

    /* 
     * Set values of the symbols to point to symbol representations in mapped 
     * memory. Only updating symbols that are pointing to section, object or 
     * function. Also avoids updating of absolute, common and undefined symbols.
     */
    unsigned sym_scanf = 0, sym_printf = 0;

    if (DEBUG_FLAG) {
        DEBUG_Elf32_Sym_HEADERS();    
    }
    
    for (i = 0; i < sh_sym_entnum; ++i) {
        short type = ELF32_ST_TYPE(sh_sym[i].st_info);
        Elf32_Section shndx = sh_sym[i].st_shndx;
        char* st_name = sh_symstr + sh_sym[i].st_name;

        if ( (type & (STT_OBJECT | STT_FUNC | STT_SECTION)) &&
             (shndx != SHN_ABS && shndx != SHN_COMMON && shndx != SHN_UNDEF)
            ) {
            sh_sym[i].st_value += shdrs[shndx].sh_addr;
        } else if (shndx == SHN_UNDEF && type == STT_NOTYPE) {
            if (strcmp(PRINTF_SYMBOL, st_name) == 0) {
                sym_printf = i;
            } else if (strcmp(SCANF_SYMBOL, st_name) == 0) {
                sym_scanf = i;
            } 
        } else if(strcmp(ENTRY_POINT, st_name) == 0) {
            sh_sym[i].st_value += shdrs[shndx].sh_addr;
        }

        /* 
         * Searches ENTRY_POINT symbol
         */
        if(strcmp(ENTRY_POINT, st_name) == 0) {
            _main = sh_sym[i].st_value;
        }

        if(DEBUG_FLAG) {
            DEBUG_Elf32_Sym(sh_sym[i], sh_symstr, i);
        }
    }

    /*
     * For each section with relocations resolves symbols and does relocations.
     * Currently processes R_ARM_CALL, R_ARM_PC24,  and R_ARM_ABS32 relocations 
     * as *P = (S + A) - P, *P = (S + A) - P and *P = S respectively. 
     */
    Elf32_Shdr* shdr_rel;

    for (i = 0; i < header.e_shnum; i += 1) {
        if (shdrs[i].sh_type == SHT_REL) {
            /*
             * Points to the relocations section header, loads table with 
             * relocations and keeps number of rows in it.
             */
            shdr_rel = &shdrs[i];
            int sh_rel_entnum = shdr_rel->sh_size / shdr_rel->sh_entsize;
            Elf32_Rel sh_rel[sh_rel_entnum];
            FREAD(sh_rel, input, shdr_rel->sh_offset, 1, shdr_rel->sh_size);

            /*
             * Address of the section which are we relocating
             */
            Elf32_Shdr* sh_info_ptr = &shdrs[shdr_rel->sh_info];

            if(SHF_EXECINSTR & sh_info_ptr->sh_flags) {
                unsigned* sh_end_ptr = sh_info_ptr->sh_addr - 
                    PAGE_OFFSET(sh_info_ptr->sh_offset) + sh_info_ptr->sh_size;

                if (DEBUG_FLAG) {
                    printf("\n\n  %x %x %x %x %x %x\n", 
                        sh_end_ptr - 4, sh_end_ptr - 3, 
                        *(sh_end_ptr - 2), *(sh_end_ptr - 1), scanf, printf);
                }

                if (sym_printf) {
                    sh_sym[sym_printf].st_value =  sh_end_ptr - 3;
                }

                if (sym_scanf) {
                    sh_sym[sym_scanf].st_value =  sh_end_ptr - 4;
                }
            }

            Elf32_Addr sh_addr = sh_info_ptr->sh_addr;

            if(DEBUG_FLAG) {
                DEBUG_Elf32_Rel_HEADERS();    
            }

            for (j = 0; j < sh_rel_entnum; ++j) {

                int type = ELF32_R_TYPE(sh_rel[j].r_info);
                int sym_indx = ELF32_R_SYM(sh_rel[j].r_info);

                unsigned* P = (unsigned*) (sh_addr + sh_rel[j].r_offset);
                unsigned S = sh_sym[sym_indx].st_value;
                unsigned A;
                
                if(DEBUG_FLAG) {
                    DEBUG_Elf32_Rel(sh_rel[j]);
                }

                switch (type) {
                    case R_ARM_PC24:;
                    case R_ARM_CALL:
                    {
                        A = 0x00FFFFFF & *P;

                        if (A & 0x00800000) {
                            A |= 0xff000000;
                        }

                         A = (S + (A << 2) - (unsigned) P) >> 2;

                        *P = (*P & 0xff000000) | (A & 0x00ffffff);
                    }
                        break;
                    case R_ARM_ABS32:
                    {
                        A = *P;
                        *P = S + A;
                    }
                }

                if (DEBUG_FLAG) {
                    printf("A: %x, P: %x, S: %x *S: %x", A, *P, S, *(unsigned *)S);
                }
            }
        }
    }
    
   
    /*
     * Sets memory access permissions according to st_flags field.
     */
    for (i = 0; i < header.e_shnum; ++i) {
        if ((SHF_ALLOC & shdrs[i].sh_flags) && shdrs[i].sh_size) {
            Elf32_Shdr* shdr_ptr = &shdrs[i];
            Elf32_Word pa_size = shdr_ptr->sh_size;
            Elf32_Word pa_addr = PAGE_ALIGN(shdr_ptr->sh_addr);
            Elf32_Word flags  = shdr_ptr->sh_flags & 0b1110;
              
            pa_size += PAGE_OFFSET(shdr_ptr->sh_offset);
            
            if (mprotect(pa_addr, pa_size, flags) == -1) {
                REPORT_ERROR("mprotect");
            }
        }
    }
    
    /**
     *  =    Write-only operand, usually used for all output operands 
     *  +	 Read-write operand, must be listed as an output operand
     *  &	 A register that should be used for output only
     */

    /* 
     * Saves current stack value and point sp register to new stack
     */
    asm("mov %[dst], sp" : [dst] "=r" (old_stack));
    asm("mov sp, %[src]" : [src] "+r" (new_stack));
    
    /* 
     * Calls main method
     */
    if(DEBUG_FLAG) {
        printf("\nSTART MAIN\n");
    }
    
    int _return = _main(argc, argv);

    if(DEBUG_FLAG) {
        printf("\nEND MAIN %x\n", _return);
    }

    /* 
     * Restores old stack's value
     */
    asm("mov sp, %[src]" : [src] "+r" (old_stack));
    

    /*
     * Removes memory mapping.
     */
    for (i = 0; i < header.e_shnum; ++i) {
        if (shdrs[i].sh_addr) {
            Elf32_Shdr* shdr_ptr = &shdrs[i];
            Elf32_Word pa_size = shdr_ptr->sh_size;

            pa_size += PAGE_OFFSET(shdr_ptr->sh_offset);

            if (munmap(PAGE_ALIGN(shdr_ptr->sh_addr), pa_size) == -1) {
                REPORT_ERROR("munmap");
            }
        }
    }

    /*
     * Closes opened files and returns from main
     */
    close(fd);
    fclose(input);
}

    printf("\n\nEND\n\n");
    
    return 0;
}